#!/bin/bash

###############################################################################
# Filename: hash.sh
# Author: Brian Lee
# Copyright: Ercom 2018
# Recursively visit directories and generate a file in each one of them.
# A generated file contains the name and hash of every file in the current directory.
###############################################################################

function usage {
  echo "Recursively visit directories and generate a '$OUTPUT' file in each one of them. A generated file contains the name and hash of every file in the current directory."
  echo -e "Usage: $0 DIRECTORY\n"
}

OUTPUT="sha256sum"
if [ $# -ne 1 ]; then
  usage
  exit 1
fi

TARGET="$1"
if [ ! -d "$TARGET" ]; then
  usage
  echo -e "ERROR: '$TARGET' is not a directory.\n"
  exit 2
fi

find "$TARGET" -type d | while read p; do
  pushd "$p" >/dev/null
  echo "Exploring $p directory..."
  sha256sum * 2>/dev/null | tee "$OUTPUT"
  popd >/dev/null
done
