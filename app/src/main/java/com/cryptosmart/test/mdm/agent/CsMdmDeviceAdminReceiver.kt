package com.cryptosmart.test.mdm.agent

import android.app.admin.DeviceAdminReceiver
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CsMdmDeviceAdminReceiver : DeviceAdminReceiver()
