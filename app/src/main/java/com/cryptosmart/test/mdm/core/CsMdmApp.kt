package com.cryptosmart.test.mdm.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author merrou issam
 * ERCOM-2019
 * Ercom mdm agent
 *
 * Base Application
 */
@HiltAndroidApp
class CsMdmApp : Application()
