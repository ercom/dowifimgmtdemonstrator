package com.cryptosmart.test.mdm.helper

import android.app.admin.DevicePolicyManager
import android.content.Context
import javax.inject.Inject

/**
 * @author merrou issam
 * ERCOM-2019
 * Ercom mdm agent
 *
 * Device policy helper
 */
class CsMdmDevicePolicyHelper @Inject constructor(
    private val appContext: Context,
    private val devicePolicyManager: DevicePolicyManager
) {

    fun isDeviceOwner() = devicePolicyManager.isDeviceOwnerApp(appContext.packageName)

}
