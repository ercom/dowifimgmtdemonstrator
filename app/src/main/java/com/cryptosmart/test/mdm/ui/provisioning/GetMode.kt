package com.cryptosmart.test.mdm.ui.provisioning

import android.annotation.SuppressLint
import android.app.admin.DevicePolicyManager
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class GetMode : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setProvisioningModeAndFinish()
    }

    // This activity will never been started in a device bellow Android Q.
    @SuppressLint("NewApi")
    private fun setProvisioningModeAndFinish() {
        val intent = Intent().apply {
            putExtra(
                DevicePolicyManager.EXTRA_PROVISIONING_MODE,
                DevicePolicyManager.PROVISIONING_MODE_FULLY_MANAGED_DEVICE
            )
        }
        setResult(RESULT_OK, intent)
        finish()
    }
}
