package com.cryptosmart.test.mdm.di

import android.app.NotificationManager
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.RestrictionsManager
import com.cryptosmart.test.mdm.helper.CsMdmDevicePolicyHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class CsMdmAppModule {

    @Provides
    fun provideDevicePolicyManager(@ApplicationContext appContext: Context) =
        appContext.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager

    @Provides
    fun provideCsMdmDevicePolicyHelper(
        @ApplicationContext appContext: Context,
        devicePolicyManager: DevicePolicyManager
    ) = CsMdmDevicePolicyHelper(appContext, devicePolicyManager)
}
