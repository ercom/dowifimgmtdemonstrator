package com.cryptosmart.test.mdm.ui.provisioning

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class Compliance : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validateProvisioning()
    }

    // This activity will never been started in a device bellow Android Q.
    private fun validateProvisioning() {
        setResult(RESULT_OK, intent)
        finish()
    }
}
