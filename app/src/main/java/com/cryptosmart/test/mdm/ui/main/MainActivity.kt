package com.cryptosmart.test.mdm.ui.main

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.cryptosmart.test.mdm.R
import com.cryptosmart.test.mdm.databinding.CsMdmContentMainBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author merrou issam
 * ERCOM-2019
 * Ercom mdm agent
 *
 * Main Activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(){

    private lateinit var binding: CsMdmContentMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CsMdmContentMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


}
