package com.cryptosmart.test.mdm.ui.main

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.cryptosmart.test.mdm.R
import com.cryptosmart.test.mdm.databinding.CsMdmSplashScreenBinding
import com.cryptosmart.test.mdm.helper.CsMdmDevicePolicyHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * @author merrou issam
 * ERCOM-2019
 * Ercom mdm agent
 *
 * Dummy splash screen
 * show useful information (is DO/PO) before use app.
 */
@AndroidEntryPoint
class MainScreen : Fragment() {

    private var _binding: CsMdmSplashScreenBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var devicePolicyHelper: CsMdmDevicePolicyHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = CsMdmSplashScreenBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadAppStatus()

        binding.enableWifi.setOnClickListener {
            val wmgr = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (wmgr.isWifiEnabled) {
                Toast.makeText(activity, "The wifi is already enabled", Toast.LENGTH_LONG).show()
            } else {
                val enabled = wmgr.setWifiEnabled(true)
                if (enabled) {
                    Toast.makeText(activity, "The wifi has been successfully enabled", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(activity, "The wifi can't be enabled", Toast.LENGTH_LONG).show()
                }
            }
        }

        binding.disableWifi.setOnClickListener {
            val wmgr = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (wmgr.isWifiEnabled) {
                val disabled = wmgr.setWifiEnabled(false)
                if (disabled) {
                    Toast.makeText(activity, "The wifi has been successfully disabled", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(activity, "The wifi can't be disabled", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(activity, "The wifi is already disabled", Toast.LENGTH_LONG).show()
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun loadAppStatus() {
        @StringRes val appStatusStringId: Int = when {
            devicePolicyHelper.isDeviceOwner() -> {
                R.string.this_is_a_device_owner
            }
            else -> {
                binding.appStatus.setTextColor(requireContext().getColor(android.R.color.holo_red_dark))
                R.string.this_is_not_an_admin
            }
        }
        binding.appStatus.text = getString(appStatusStringId)
    }
}
