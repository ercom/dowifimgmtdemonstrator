import Depends.androidxNavigation
import Depends.hilt
import Depends.kotlinCoroutines

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("dagger.hilt.android.plugin")
}
apply(from = "${project.rootDir}/ktlint.gradle.kts")

android {

    compileSdk = ProjectConfig.compileSdkVersion
    buildToolsVersion = ProjectConfig.buildToolsVersion

    defaultConfig {
        applicationId = ProjectConfig.applicationId
        minSdk = ProjectConfig.minSdkVersion
        targetSdk = ProjectConfig.targetSdkVersion
        versionCode = BuildUtils.getVersionCode(project)
        versionName = BuildUtils.getBuildName(project)
        base.archivesName.set("ercom-mdm-agent")
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        create("release") {
            storeFile = file(project.property("KEYSTORE_FILE_PATH") as String)
            storePassword = project.property("STORE_PASS") as String
            keyAlias = project.property("KEY_ALIAS") as String
            keyPassword = project.property("KEY_PASSWORD") as String
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("release")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("debug") {
            isMinifyEnabled = false
            isDebuggable = true
            signingConfig = signingConfigs.getByName("release")
        }
    }

    flavorDimensions("version")

    productFlavors {
        create("ercom") {
            dimension = "version"
            applicationIdSuffix = ".rd"
            versionNameSuffix = "-R&D"
        }
        create("client") {
            dimension = "version"
            applicationIdSuffix = ".client"
            versionNameSuffix = "-Client"
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    packagingOptions {
        resources.pickFirsts.addAll(
            arrayOf(
                "META-INF/kotlinx-coroutines-core.kotlin_module",
                "META-INF/kotlinx-io.kotlin_module",
                "META-INF/atomicfu.kotlin_module",
                "META-INF/kotlinx-coroutines-io.kotlin_module"
            )
        )
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
}

dependencies {

    // kotlin stdlib
    implementation(Depends.kotlin_stdlib_jdk8)
    // kotlin coroutine
    kotlinCoroutines()

    implementation(Depends.androidxCore)
    implementation(Depends.timber)
    implementation(Depends.constraintlayout)
    implementation(Depends.androidxAnnotation)
    implementation(Depends.androidMaterial)
    implementation(Depends.androidxActivityKtx)
    implementation(Depends.androidxFragmentKtx)
    implementation(Depends.androidxAppcompat)
    androidxNavigation()
    implementation(Depends.viewmodelKtx)
    implementation(Depends.livedataKtx)
    implementation(Depends.recyclerview)
    hilt()
    implementation(Depends.cardview)
}
kapt {
    correctErrorTypes = true
}
