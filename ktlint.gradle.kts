val ktlint: Configuration by configurations.creating

dependencies {
    ktlint(Depends.ktlint)
}

tasks.register<JavaExec>("ktlint") {
    group = "verification"
    description = "Check Kotlin code style."
    classpath = ktlint
    main =  "com.pinterest.ktlint.Main"
    args("src/**/*.kt", "build.gradle.kts")
}

tasks.named("check") {
    dependsOn(ktlint)
}

tasks.register<JavaExec>("ktlintFormat") {
    group = "formatting"
    description = "Fix Kotlin code style deviations."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args("-F", "src/**/*.kt", "build.gradle.kts")
}