/**
 * application dependencies version
 */
object Versions {

    const val ktlint = "0.40.0"

    // constraint layout
    const val constraintlayout = "2.1.3"

    // material design
    const val material = "1.6.0"

    // kotlin
    const val kotlin = "1.6.21"

    // coroutine
    const val kotlin_coroutine = "1.6.0"

    // app compact support
    const val androidx_appcompat = "1.4.1"

    // androidx fragment
    const val androidx_fragment = "1.4.1"

    // androidx activity
    const val androidx_activity = "1.4.0"

    const val androidx_core = "1.7.0"

    // annotation
    const val androidx_annotation = "1.3.0"

    // lifecycle
    const val androidx_lifecycle = "2.4.0"

    // timber version
    const val timber = "4.7.1"

    const val androidx_navigation = "2.4.2"

    const val recyclerview = "1.2.1"

    const val hilt = "1.0.0"
    const val hilt_lifecycle = "1.0.0-alpha03"
    const val hilt_pluging = "2.38.1"

    const val cardView = "1.0.0"
}
