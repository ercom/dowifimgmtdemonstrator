/**
 * Nexus configuration
 */
object NexusConfig {

    fun externalUri() = NEXUS_HOST + NEXUS_EXTERNAL_REPOSITORY
    fun internalUri() = NEXUS_HOST + NEXUS_LOCAL_REPOSITORY

    // Nexus configuration
    private const val NEXUS_HOST = "https://nexus.cryptosmart.com"
    private const val NEXUS_EXTERNAL_REPOSITORY = "/repository/maven-externals/"
    private const val NEXUS_LOCAL_REPOSITORY = "/repository/cryptosmart_android_repository/"
}
