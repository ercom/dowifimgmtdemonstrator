/**
 * application configuration
 */
object ProjectConfig {

    const val applicationId = "com.cryptosmart.test.mdm"
    const val minSdkVersion = 26
    const val targetSdkVersion = 32
    const val compileSdkVersion = 32
    const val buildToolsVersion = "32.0.0"
}
