import org.gradle.api.Project
import java.io.ByteArrayOutputStream

/**
 * Utils class
 */
object BuildUtils {

    fun getBuildName(project: Project): String {
        val isUpdateTestBuild: String? = System.getenv("UPDATE_TEST")
        return if (isUpdateTestBuild == "yes") {
            println("Using version for update testing")
            execCmd(project, "/bin/sh ../version.sh --update-test")
        } else {
            execCmd(project, "/bin/sh ../version.sh")
        }
    }

    fun getVersionCode(project: Project): Int {
        val isUpdateTestBuild = System.getenv("UPDATE_TEST")
        val internalVersion = if (isUpdateTestBuild == "yes") {
            println("Using version for update testing")
            execCmd(project, "/bin/sh ../version.sh --update-test --internal".trim())
        } else {
            execCmd(project, "/bin/sh ../version.sh --internal")
        }
        return Integer.parseInt(internalVersion)
    }

    private fun execCmd(project: Project, cmd: String): String {
        val byteOut = ByteArrayOutputStream()
        project.exec {
            commandLine = cmd.split(" ")
            standardOutput = byteOut
        }
        return String(byteOut.toByteArray())
    }
}
