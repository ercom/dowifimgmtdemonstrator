import org.gradle.api.artifacts.dsl.DependencyHandler

/**
 * application dependencies
 */
object Depends {

    const val ktlint = "com.pinterest:ktlint:${Versions.ktlint}"

    const val kotlin_stdlib_jdk8 =
        "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"

    fun DependencyHandler.kotlinCoroutines() {
        listOf(
            "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlin_coroutine}",
            "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlin_coroutine}"
        ).forEach { add("implementation", it) }
    }

    fun DependencyHandler.androidxNavigation() {
        listOf(
            "androidx.navigation:navigation-fragment-ktx:${Versions.androidx_navigation}",
            "androidx.navigation:navigation-ui-ktx:${Versions.androidx_navigation}"
        ).forEach { add("implementation", it) }
    }

    // androidx core
    const val androidxCore = "androidx.core:core-ktx:${Versions.androidx_core}"

    // timber
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    // app compact
    const val androidxAppcompat =
        "androidx.appcompat:appcompat:${Versions.androidx_appcompat}"

    // androidx fragment
    const val androidxFragmentKtx =
        "androidx.fragment:fragment-ktx:${Versions.androidx_fragment}"

    // androidx activity
    const val androidxActivityKtx =
        "androidx.activity:activity-ktx:${Versions.androidx_activity}"

    // android material
    const val androidMaterial =
        "com.google.android.material:material:${Versions.material}"

    // android annotation
    const val androidxAnnotation =
        "androidx.annotation:annotation:${Versions.androidx_annotation}"

    // android constraint layout
    const val constraintlayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintlayout}"

    //lifecycle
    const val viewmodelKtx =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.androidx_lifecycle}"
    const val livedataKtx =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.androidx_lifecycle}"

    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerview}"

    fun DependencyHandler.hilt() {
        listOf(
            "com.google.dagger:hilt-android:${Versions.hilt_pluging}",
            "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hilt_lifecycle}"
        ).forEach { add("implementation", it) }
        listOf(
            "com.google.dagger:hilt-android-compiler:${Versions.hilt_pluging}",
            "androidx.hilt:hilt-compiler:${Versions.hilt}"
        ).forEach { add("kapt", it) }
    }

    const val cardview = "androidx.cardview:cardview:${Versions.cardView}"
}
